---
acronym: lecturer
type: stakeholderRole
isTemplate: true
author:
    - fkr
    - sbe
name: Lecturer
description: Lecturer at a university (not necessarily a professor)
sources:
    - reference: [literatureReference, rouse2016, "p. 167"]
      usedFor: Additional reference to show that a role like "lecturer" exists
history:
    v1:
        date: 2021-07-22
        comment: initially created
    v2: 
        date: 2021-07-27
        comment: changed into top Level role and renamed to "lehrende", to keep name konsistent (sbe)
todo:
ignore: 
---

## Profile

Lecturer is an academic rank within many universities, though the meaning of the term varies somewhat from country to 
country. It generally denotes an academic expert who is hired to teach on a full- or part-time basis. 
They may also conduct research.