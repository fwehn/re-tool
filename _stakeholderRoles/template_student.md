---
acronym: student
type: stakeholderRole
isTemplate: true
author: 
    - sbe
name: Student
description: > 
    Student in a somewhat technical study program, where exercises and practicals consist of applying methods
    and tools to given problems
history:
    v1:
        date: 2021-05-04
        comment: initially created
    v2:
        date: 2022-05-12
        comment: Adapted as template
todo:
ignore: 
---

## Role Description

This is an active user of the system, as a student. He/she usually uses Divekit during an exercise or a practical, 
as required by the lecturer. The student is interested in good usability, so that the solution of his/her tasks remains 
as simple as possible. Typical tasks are:

* Read exercise tasks
* Provide solutions at a time
* Requesting feedback / advice from teachers
* Write exams

The student is familiar with using IT tools such as Git, IDEA, GitLab.